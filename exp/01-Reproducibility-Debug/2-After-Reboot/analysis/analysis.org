#+TITLE: Pinning analysis
#+AUTHOR: Christopher Ferreira

* Gather data

#+BEGIN_SRC sh
  export LANG=C

  datafile=data
  rm $datafile
  for exp in ../outputs/*; do
      program=$(echo $(basename $exp) | cut -d'-' -f1)
      pinning=$(echo $(basename $exp) | cut -d'-' -f2)
      nodes=$(echo $(basename $exp) | cut -d'-' -f3)
      autogroup=$(echo $(basename $exp) | cut -d'-' -f4)

      for iteration_dir in $exp/*; do
          iteration=$(basename $iteration_dir)
          time=$(tail -1 $iteration_dir/time)
          printf '%s,%s,%d,%s,%d,%f\n' \
                  $program $pinning $nodes $autogroup $iteration $time \
                  >>$datafile
      done
  done

  wc -l $datafile
#+END_SRC

#+RESULTS:
: 896 data

* Time Plot
  :PROPERTIES:
  :colnames: yes
  :session:  *02-pinningtime*
  :width:    800
  :height:   600
  :units:    px
  :END:

** Load libraries

#+BEGIN_SRC R :colnames no :results value verbatim
  library('tidyverse')

  'Ok !'
#+END_SRC

#+RESULTS:
: Ok !

** Load data

#+BEGIN_SRC R
  data <- read_csv('data',
          col_names = c('Program', 'Pinning', 'Nodes', 'Autogroup', 'Iteration', 'Time'))
  summary(data)
#+END_SRC

#+RESULTS:
| Program          | Pinning          | Nodes     | Autogroup        | Iteration    | Time          |
|------------------+------------------+-----------+------------------+--------------+---------------|
| Length:896       | Length:896       | Min.   :2 | Length:896       | Min.   :1.00 | Min.   : 1.32 |
| Class :character | Class :character | 1st Qu.:3 | Class :character | 1st Qu.:2.75 | 1st Qu.: 7.51 |
| Mode  :character | Mode  :character | Median :5 | Mode  :character | Median :4.50 | Median :13.06 |
| nil              | nil              | Mean   :5 | nil              | Mean   :4.50 | Mean   :13.93 |
| nil              | nil              | 3rd Qu.:7 | nil              | 3rd Qu.:6.25 | 3rd Qu.:20.76 |
| nil              | nil              | Max.   :8 | nil              | Max.   :8.00 | Max.   :38.12 |

** Process Data

#+BEGIN_SRC R
  processed <- data %>%
    select(Program, Pinning, Nodes, Autogroup, Time) %>%
    group_by(Program, Pinning, Nodes, Autogroup) %>%
    summarise(
      TimeMean = mean(Time),
      TimeSd   = sd(Time),
      TimeMin  = min(Time),
      TimeMax  = max(Time)
    )
#+END_SRC

#+RESULTS:
| Program   | Pinning | Nodes | Autogroup | TimeMean |              TimeSd | TimeMin | TimeMax |
|-----------+---------+-------+-----------+----------+---------------------+---------+---------|
| bodytrack | cores   |     2 | n         |    37.51 |  0.0282842712474635 |   37.47 |   37.56 |
| bodytrack | cores   |     3 | n         |    31.15 |   0.065027466724234 |   31.01 |   31.22 |
| bodytrack | cores   |     4 | n         |    28.21 |   0.114267855747549 |   28.01 |   28.36 |
| bodytrack | cores   |     5 | n         | 26.87125 |   0.500697727462103 |   26.59 |    28.1 |
| bodytrack | cores   |     6 | n         |    26.12 |  0.0834950811211574 |   26.02 |   26.26 |
| bodytrack | cores   |     7 | n         |    25.79 |  0.0661167798023211 |   25.73 |   25.89 |
| bodytrack | cores   |     8 | n         | 25.89875 |  0.0962789845041111 |   25.71 |   26.03 |
| bodytrack | nodes   |     2 | n         | 36.93625 |   0.720454965381498 |   36.32 |   38.12 |
| bodytrack | nodes   |     3 | n         |     30.2 |   0.131257652838116 |   30.01 |   30.35 |
| bodytrack | nodes   |     4 | n         |    27.37 |   0.441102514809686 |   27.02 |   28.32 |
| bodytrack | nodes   |     5 | n         |  25.7575 |   0.208515158750081 |   25.36 |   26.04 |
| bodytrack | nodes   |     6 | n         |  25.0275 |   0.159798980865694 |   24.79 |   25.26 |
| bodytrack | nodes   |     7 | n         |   24.565 |   0.179682259240344 |   24.37 |   24.82 |
| bodytrack | nodes   |     8 | n         |    24.56 |   0.533720097857615 |   24.05 |   25.72 |
| bodytrack | none    |     2 | n         | 34.55875 |   0.900197398986944 |   33.73 |   36.64 |
| bodytrack | none    |     3 | n         | 28.35375 |   0.314230920548203 |   28.06 |   29.08 |
| bodytrack | none    |     4 | n         | 25.44625 |  0.0954594154601839 |   25.27 |   25.56 |
| bodytrack | none    |     5 | n         | 24.07375 |   0.112749722837797 |   23.84 |   24.17 |
| bodytrack | none    |     6 | n         |  23.3725 |   0.181560694298864 |   23.07 |   23.65 |
| bodytrack | none    |     7 | n         | 23.02625 |  0.0962047667366706 |   22.82 |    23.1 |
| bodytrack | none    |     8 | n         | 23.43625 |   0.421118832906546 |   23.12 |   24.44 |
| bodytrack | subset  |     2 | n         |  37.1775 |   0.113860817291488 |      37 |   37.35 |
| bodytrack | subset  |     3 | n         |    29.88 |   0.218893320396685 |   29.39 |    30.1 |
| bodytrack | subset  |     4 | n         |  26.4725 |  0.0937702359112803 |   26.33 |   26.59 |
| bodytrack | subset  |     5 | n         | 24.71125 |    0.15869445034855 |   24.51 |      25 |
| bodytrack | subset  |     6 | n         |  23.9625 |   0.207553779605603 |   23.73 |   24.23 |
| bodytrack | subset  |     7 | n         |  23.5325 |   0.264885419973024 |   23.32 |   24.15 |
| bodytrack | subset  |     8 | n         | 23.52875 |   0.415844665367113 |   23.14 |   24.42 |
| histogram | cores   |     2 | n         |  1.73625 |  0.0686476510887299 |    1.69 |     1.9 |
| histogram | cores   |     3 | n         |  1.46125 | 0.00991031208965116 |    1.45 |    1.48 |
| histogram | cores   |     4 | n         |  1.36875 |   0.018077215335491 |    1.34 |     1.4 |
| histogram | cores   |     5 | n         |  1.42125 |  0.0135620268186054 |    1.41 |    1.45 |
| histogram | cores   |     6 | n         |    1.555 | 0.00925820099772552 |    1.54 |    1.56 |
| histogram | cores   |     7 | n         |  1.67625 |  0.0106066017177982 |    1.67 |     1.7 |
| histogram | cores   |     8 | n         |  1.83625 | 0.00744023809142846 |    1.83 |    1.85 |
| histogram | nodes   |     2 | n         |   2.0025 |   0.138125202003731 |    1.79 |    2.19 |
| histogram | nodes   |     3 | n         |   1.7425 |    0.37170456778776 |    1.46 |    2.38 |
| histogram | nodes   |     4 | n         |     1.79 |   0.308683841957616 |    1.46 |    2.33 |
| histogram | nodes   |     5 | n         |  1.87125 |   0.349793306315111 |     1.5 |    2.63 |
| histogram | nodes   |     6 | n         |  1.97875 |   0.304276072952555 |    1.59 |    2.36 |
| histogram | nodes   |     7 | n         |  1.77375 |  0.0509726817590981 |    1.71 |    1.86 |
| histogram | nodes   |     8 | n         |  2.24375 |   0.291005031866166 |    1.91 |     2.6 |
| histogram | none    |     2 | n         |     1.48 |   0.135541243064348 |    1.32 |    1.72 |
| histogram | none    |     3 | n         |    1.665 |   0.177924542593603 |    1.51 |    2.07 |
| histogram | none    |     4 | n         |   1.6225 |   0.121390750412518 |    1.48 |    1.85 |
| histogram | none    |     5 | n         |    1.725 |  0.0834950811211569 |    1.61 |    1.85 |
| histogram | none    |     6 | n         |  1.82375 |  0.0459619407771256 |    1.75 |     1.9 |
| histogram | none    |     7 | n         |    1.995 |  0.0570713838726805 |    1.92 |    2.09 |
| histogram | none    |     8 | n         |  2.21125 |  0.0571807909803883 |    2.13 |    2.26 |
| histogram | subset  |     2 | n         |  2.26375 |   0.231759573696536 |    1.96 |    2.65 |
| histogram | subset  |     3 | n         |  2.16875 |   0.224336322770713 |    1.76 |    2.52 |
| histogram | subset  |     4 | n         |  2.29375 |   0.254779316271945 |    1.74 |    2.59 |
| histogram | subset  |     5 | n         |    1.895 |  0.0907114735222145 |    1.82 |    2.04 |
| histogram | subset  |     6 | n         |  1.99875 |   0.142872320622295 |    1.81 |    2.27 |
| histogram | subset  |     7 | n         |     2.05 |   0.050709255283711 |    1.97 |    2.11 |
| histogram | subset  |     8 | n         |    2.195 |  0.0542481072744216 |    2.11 |    2.27 |
| volrend   | cores   |     2 | n         | 17.23375 |  0.0560452623204807 |   17.16 |   17.35 |
| volrend   | cores   |     3 | n         | 13.97875 |  0.0145773797371129 |   13.96 |      14 |
| volrend   | cores   |     4 | n         | 12.79625 |  0.0266926956300785 |   12.75 |   12.84 |
| volrend   | cores   |     5 | n         | 12.50375 |  0.0297309362689532 |   12.46 |   12.55 |
| volrend   | cores   |     6 | n         |    12.96 |  0.0362530786869986 |    12.9 |   13.02 |
| volrend   | cores   |     7 | n         | 13.61625 |  0.0495515604482555 |   13.55 |   13.69 |
| volrend   | cores   |     8 | n         |  14.4125 |  0.0774135462490714 |   14.31 |   14.54 |
| volrend   | nodes   |     2 | n         | 17.34375 |  0.0547559520574185 |   17.27 |   17.44 |
| volrend   | nodes   |     3 | n         |  14.1775 |  0.0471320333894961 |   14.12 |   14.26 |
| volrend   | nodes   |     4 | n         |   13.145 |   0.060710083888217 |   13.04 |   13.22 |
| volrend   | nodes   |     5 | n         | 12.83375 |  0.0720986427452994 |   12.74 |   12.92 |
| volrend   | nodes   |     6 | n         |  13.3125 |  0.0884388408530358 |   13.18 |   13.44 |
| volrend   | nodes   |     7 | n         |  14.1075 |  0.0565053726901888 |   14.02 |   14.18 |
| volrend   | nodes   |     8 | n         | 15.07875 |  0.0714017806900805 |   14.95 |   15.15 |
| volrend   | none    |     2 | n         |  17.2225 |   0.123027290816771 |   17.06 |   17.47 |
| volrend   | none    |     3 | n         | 14.09375 |   0.041726148019814 |   14.04 |   14.15 |
| volrend   | none    |     4 | n         | 13.08375 |  0.0728868986855665 |   13.01 |   13.24 |
| volrend   | none    |     5 | n         | 13.31625 |  0.0967969155352735 |   13.18 |   13.45 |
| volrend   | none    |     6 | n         |   13.675 |  0.0851888658050033 |    13.6 |   13.86 |
| volrend   | none    |     7 | n         | 14.54625 |   0.125121369649062 |   14.38 |   14.72 |
| volrend   | none    |     8 | n         | 15.85125 |   0.256260888270417 |   15.48 |   16.19 |
| volrend   | subset  |     2 | n         | 17.62625 |  0.0874948978104277 |   17.52 |   17.76 |
| volrend   | subset  |     3 | n         | 15.20375 |   0.640132519762232 |    14.6 |   16.67 |
| volrend   | subset  |     4 | n         |  14.2925 |    0.73129532826544 |   13.57 |   15.94 |
| volrend   | subset  |     5 | n         |  14.1475 |    1.67211371793736 |   13.15 |   18.17 |
| volrend   | subset  |     6 | n         |  15.5275 |    1.70631390513503 |   13.56 |   18.67 |
| volrend   | subset  |     7 | n         | 15.48625 |   0.534974966036463 |   14.67 |   16.28 |
| volrend   | subset  |     8 | n         | 15.67875 |   0.232528800059569 |   15.29 |    15.9 |
| word_count | cores   |     2 | n         |   9.1925 |  0.0271240536372106 |    9.13 |    9.22 |
| word_count | cores   |     3 | n         |  9.54875 |  0.0538350655773196 |    9.49 |    9.65 |
| word_count | cores   |     4 | n         |  9.93875 |  0.0516685867538761 |    9.88 |   10.02 |
| word_count | cores   |     5 | n         | 10.15375 |  0.0399776723398736 |   10.12 |   10.24 |
| word_count | cores   |     6 | n         |  10.4225 |  0.0468279525802393 |   10.34 |   10.48 |
| word_count | cores   |     7 | n         | 10.62875 |  0.0285043856274786 |   10.59 |   10.68 |
| word_count | cores   |     8 | n         | 11.14375 |  0.0544944295973713 |   11.07 |   11.21 |
| word_count | nodes   |     2 | n         |   10.475 |    1.08902315336793 |    9.26 |   12.67 |
| word_count | nodes   |     3 | n         | 12.55125 |    1.85817450741312 |    9.46 |   16.16 |
| word_count | nodes   |     4 | n         | 11.82375 |   0.565355450769059 |   11.19 |   12.65 |
| word_count | nodes   |     5 | n         | 11.45125 |    1.38812759294155 |   10.48 |   14.78 |
| word_count | nodes   |     6 | n         | 10.71625 |   0.308773495346624 |   10.37 |   11.12 |
| word_count | nodes   |     7 | n         | 11.25625 |   0.451914577895298 |   10.78 |   12.15 |
| word_count | nodes   |     8 | n         | 11.93125 |   0.913915243022334 |   11.35 |   14.09 |
| word_count | none    |     2 | n         |  11.4075 |    1.72438601909698 |   10.06 |   15.06 |
| word_count | none    |     3 | n         | 12.17625 |   0.725631498371296 |   10.79 |   13.09 |
| word_count | none    |     4 | n         | 12.39625 |   0.559309204031442 |   11.47 |   13.07 |
| word_count | none    |     5 | n         | 11.55125 |   0.259363919728907 |   11.14 |   12.02 |
| word_count | none    |     6 | n         |  11.4375 |   0.398129555367525 |   10.91 |   11.97 |
| word_count | none    |     7 | n         | 11.51375 |   0.135006613594615 |   11.27 |   11.69 |
| word_count | none    |     8 | n         | 11.99125 |   0.102041658159793 |   11.82 |   12.16 |
| word_count | subset  |     2 | n         |   18.205 |    1.19860633356292 |   16.74 |   20.07 |
| word_count | subset  |     3 | n         | 16.84125 |   0.830188231667975 |   15.89 |   17.83 |
| word_count | subset  |     4 | n         |   14.815 |   0.305801055776931 |   14.33 |   15.42 |
| word_count | subset  |     5 | n         | 13.14625 |   0.365589055009661 |   12.46 |   13.73 |
| word_count | subset  |     6 | n         |  12.5075 |    0.40665007772566 |   11.86 |   13.03 |
| word_count | subset  |     7 | n         |  11.8975 |   0.263045351874647 |   11.61 |   12.35 |
| word_count | subset  |     8 | n         | 12.12625 |   0.189204160328769 |   11.78 |   12.27 |

** Plot

#+BEGIN_SRC R :results output graphics :file time.png
  ggplot(processed,
    x = Nodes,
    y = TimeMean
  ) +
    geom_line(aes(
      x = Nodes,
      y = TimeMean,
      colour = Pinning
    )) +
    geom_pointrange(aes(
      x = Nodes,
      y = TimeMean,
      ymin = TimeMin,
      ymax = TimeMax,
      colour = Pinning
    )) +
    facet_wrap(~ Program, scales = "free") +
    theme(legend.position = "bottom") +
    scale_x_continuous(breaks = seq(2, 8, by=1)) +
    ylim(0, NA)
#+END_SRC

#+RESULTS:
[[file:time.png]]

* Speedup Plot
  :PROPERTIES:
  :colnames: yes
  :session:  *02-pinningspeedup*
  :width:    800
  :height:   600
  :units:    px
  :END:

** Load libraries

#+BEGIN_SRC R :colnames no :results value verbatim
  library('tidyverse')

  'Ok !'
#+END_SRC

#+RESULTS:
: Ok !

** Load data

#+BEGIN_SRC R
  data <- read_csv('data',
          col_names = c('Program', 'Pinning', 'Nodes', 'Autogroup', 'Iteration', 'Time'))
  summary(data)
#+END_SRC

#+RESULTS:
| Program          | Pinning          | Nodes     | Autogroup        | Iteration    | Time          |
|------------------+------------------+-----------+------------------+--------------+---------------|
| Length:896       | Length:896       | Min.   :2 | Length:896       | Min.   :1.00 | Min.   : 1.32 |
| Class :character | Class :character | 1st Qu.:3 | Class :character | 1st Qu.:2.75 | 1st Qu.: 7.51 |
| Mode  :character | Mode  :character | Median :5 | Mode  :character | Median :4.50 | Median :13.06 |
| nil              | nil              | Mean   :5 | nil              | Mean   :4.50 | Mean   :13.93 |
| nil              | nil              | 3rd Qu.:7 | nil              | 3rd Qu.:6.25 | 3rd Qu.:20.76 |
| nil              | nil              | Max.   :8 | nil              | Max.   :8.00 | Max.   :38.12 |

** Process Data

#+BEGIN_SRC R
  relevant_data <- data %>%
    select(Program, Pinning, Nodes, Autogroup, Time)

  cores <- relevant_data %>%
    filter(Pinning == 'cores') %>%
    rename(TimePinned = Time)

  nodes <- relevant_data %>%
    filter(Pinning == 'nodes') %>%
    rename(TimePinned = Time)

  speedup_cores <- relevant_data %>%
    filter(Pinning == c('subset', 'none')) %>%
    rename(Config = Pinning) %>%
    left_join(cores) %>%
    mutate(Speedup = Time / TimePinned) %>%
    group_by(Program, Pinning, Config, Nodes, Autogroup) %>%
    summarise(
       SpeedupMean = mean(Speedup),
       SpeedupSd   = sd(Speedup),
       SpeedupMin  = min(Speedup),
       SpeedupMax  = max(Speedup)
    )

  speedup_nodes <- relevant_data %>%
    filter(Pinning == c('subset', 'none')) %>%
    rename(Config = Pinning) %>%
    left_join(nodes) %>%
    mutate(Speedup = Time / TimePinned) %>%
    group_by(Program, Pinning, Config, Nodes, Autogroup) %>%
    summarise(
       SpeedupMean = mean(Speedup),
       SpeedupSd   = sd(Speedup),
       SpeedupMin  = min(Speedup),
       SpeedupMax  = max(Speedup)
    )

  processed <- union(speedup_cores, speedup_nodes)
#+END_SRC

#+RESULTS:
| Program   | Pinning | Config | Nodes | Autogroup |       SpeedupMean |           SpeedupSd |        SpeedupMin |        SpeedupMax |
|-----------+---------+--------+-------+-----------+-------------------+---------------------+-------------------+-------------------|
| word_count | nodes   | subset |     8 | n         |  1.01117839845012 |  0.0669628071876866 | 0.836053938963804 |  1.07488986784141 |
| word_count | nodes   | subset |     7 | n         |  1.07642196828255 |   0.043237086382943 | 0.978600823045268 |   1.1456400742115 |
| word_count | nodes   | subset |     5 | n         |  1.15351502705023 |   0.116840720555448 | 0.843031123139378 |  1.31011450381679 |
| word_count | nodes   | subset |     4 | n         |  1.26731617775888 |  0.0617937278207151 |  1.16126482213439 |  1.37801608579088 |
| word_count | nodes   | subset |     3 | n         |  1.34513219967581 |    0.20023733880249 | 0.986386138613861 |  1.87103594080338 |
| word_count | nodes   | subset |     2 | n         |  1.68448035812132 |   0.163111793449391 |  1.32123125493291 |  1.96004319654428 |
| word_count | nodes   | none   |     8 | n         |  1.00970499112515 |  0.0651157646013383 | 0.844570617459191 |  1.06343612334802 |
| word_count | nodes   | none   |     7 | n         |  1.02682649329763 |   0.038788754220179 | 0.939917695473251 |  1.08441558441558 |
| word_count | nodes   | none   |     6 | n         |  1.07694699801235 |  0.0462571003642022 | 0.982913669064748 |   1.1542912246866 |
| word_count | nodes   | none   |     5 | n         |  1.00811211908808 |  0.0973926616673864 | 0.753721244925575 |  1.12022900763359 |
| word_count | nodes   | none   |     4 | n         |  1.06012205842619 |  0.0593817101707301 |  0.95098814229249 |  1.16800714924039 |
| word_count | nodes   | none   |     3 | n         |   1.0036209000751 |   0.146760763874401 | 0.745049504950495 |  1.37420718816068 |
| word_count | nodes   | none   |     2 | n         |  1.12033835139201 |   0.221706184109614 | 0.797947908445146 |  1.62634989200864 |
| volrend   | nodes   | subset |     8 | n         |  1.05017402960668 | 0.00570763601389776 |  1.04092409240924 |  1.06354515050167 |
| volrend   | nodes   | subset |     7 | n         |  1.08631687677973 |  0.0189972205561303 |  1.06064880112835 |  1.11412268188302 |
| volrend   | nodes   | subset |     6 | n         |  1.23535287258413 |   0.126044448087986 |  1.05803571428571 |   1.4165402124431 |
| volrend   | nodes   | subset |     5 | n         |  1.14895549088565 |   0.160112387874378 |  1.01780185758514 |  1.42621664050235 |
| volrend   | nodes   | subset |     4 | n         |  1.06068928139533 |  0.0182841346599131 |  1.02647503782148 |  1.08435582822086 |
| volrend   | nodes   | subset |     3 | n         |  1.05608495883712 |  0.0161884463988112 |  1.02384291725105 |  1.07507082152975 |
| volrend   | nodes   | subset |     2 | n         |  1.01550434122081 | 0.00347434707776248 |  1.00860091743119 |  1.02258251302837 |
| volrend   | nodes   | none   |     8 | n         |  1.04486844562382 |  0.0190792696237298 |  1.02178217821782 |  1.07826086956522 |
| volrend   | nodes   | none   |     6 | n         |  1.02764532058078 |  0.0103044622063819 |  1.01190476190476 |    1.051593323217 |
| volrend   | nodes   | none   |     5 | n         |   1.0416183468575 |  0.0067230044395564 |  1.03250773993808 |  1.05572998430141 |
| volrend   | nodes   | none   |     4 | n         | 0.997165842272859 | 0.00808252811568461 |  0.98411497730711 |  1.01533742331288 |
| volrend   | nodes   | none   |     3 | n         | 0.995072202824822 | 0.00413901548254493 | 0.985273492286115 |  1.00212464589235 |
| volrend   | nodes   | none   |     2 | n         |  0.99373838585894 | 0.00917111602765947 | 0.978211009174312 |  1.01158077591199 |
| histogram | nodes   | subset |     8 | n         | 0.997349741082959 |   0.125620565026285 | 0.811538461538461 |  1.18848167539267 |
| histogram | nodes   | subset |     7 | n         |  1.16503741425927 |  0.0370413058755487 |  1.08064516129032 |   1.2280701754386 |
| histogram | nodes   | subset |     6 | n         |  1.05853306399161 |    0.17577106239068 | 0.800847457627119 |  1.42767295597484 |
| histogram | nodes   | subset |     5 | n         |  1.03521204671942 |   0.164579223167169 | 0.692015209125475 |              1.36 |
| histogram | nodes   | subset |     4 | n         |   1.3206923784164 |   0.208938753482606 | 0.927038626609442 |  1.65068493150685 |
| histogram | nodes   | subset |     3 | n         |  1.26320365517236 |   0.277770280410414 | 0.739495798319328 |  1.72602739726027 |
| histogram | nodes   | subset |     2 | n         |   1.1321850815262 |    0.10839646489467 | 0.963470319634703 |  1.39106145251397 |
| histogram | nodes   | none   |     8 | n         |  1.00526521521854 |   0.125588130686788 | 0.823076923076923 |  1.18324607329843 |
| histogram | nodes   | none   |     7 | n         |  1.11426096521165 |  0.0380921219062952 |  1.03225806451613 |  1.18713450292398 |
| histogram | nodes   | none   |     6 | n         | 0.938479923807199 |   0.143597968468914 | 0.741525423728814 |  1.16981132075472 |
| histogram | nodes   | none   |     5 | n         | 0.948830114344158 |   0.150413781022438 | 0.612167300380228 |  1.20666666666667 |
| histogram | nodes   | none   |     4 | n         | 0.941512009748633 |   0.165196840973945 |  0.63519313304721 |  1.26712328767123 |
| histogram | nodes   | none   |     3 | n         |  1.02691026555777 |   0.218727911817769 | 0.663865546218487 |  1.41780821917808 |
| bodytrack | nodes   | subset |     8 | n         | 0.953049829476061 |  0.0220742093874431 | 0.899688958009331 |  0.99002079002079 |
| bodytrack | nodes   | subset |     7 | n         | 0.961677328123076 |  0.0145992895965428 | 0.941579371474617 |  0.99097250718096 |
| bodytrack | nodes   | subset |     6 | n         | 0.956781684305491 | 0.00918167620200163 | 0.940221694378464 | 0.976200080677693 |
| bodytrack | nodes   | subset |     5 | n         | 0.959678690703871 |  0.0103788757481236 | 0.941244239631337 | 0.985804416403785 |
| bodytrack | nodes   | subset |     4 | n         | 0.966601748004516 |  0.0147234845052061 |  0.93114406779661 | 0.982235381199112 |
| bodytrack | nodes   | subset |     3 | n         | 0.988923641960897 |  0.0103306064081291 |  0.96836902800659 |  1.00299900033322 |
| histogram | nodes   | none   |     2 | n         | 0.781119939967688 |  0.0824613636371952 | 0.634703196347032 | 0.960893854748603 |
| bodytrack | nodes   | subset |     2 | n         |  1.00638648840954 |  0.0185403750173405 | 0.970619097586569 |  1.02670704845815 |
| bodytrack | nodes   | none   |     8 | n         | 0.945106898960073 |   0.019125749036632 | 0.898911353032659 | 0.967151767151767 |
| bodytrack | nodes   | none   |     7 | n         | 0.935419231958852 | 0.00774141874989748 |  0.91941982272361 | 0.947886745999179 |
| bodytrack | nodes   | none   |     6 | n         |  0.93310688171827 | 0.00728443816790447 | 0.917656373713381 | 0.947156111335216 |
| bodytrack | nodes   | none   |     5 | n         | 0.937159680261543 | 0.00729338705710774 | 0.925883256528418 |  0.95307570977918 |
| bodytrack | nodes   | none   |     4 | n         | 0.930970870715124 |  0.0142830354133931 | 0.896539548022599 | 0.945965951147298 |
| bodytrack | nodes   | none   |     2 | n         | 0.930623135306047 |  0.0179719967022019 | 0.894805876180483 |  0.95374449339207 |
| word_count | cores   | subset |     8 | n         |   1.0777567246161 |   0.017836978065524 |  1.05084745762712 |  1.10207768744354 |
| word_count | cores   | subset |     7 | n         |  1.13842889189226 |  0.0185283118215556 |  1.11329588014981 |  1.16619452313503 |
| word_count | cores   | subset |     6 | n         |  1.19839012983213 |   0.038775909951209 |  1.13167938931298 |   1.2495164410058 |
| word_count | cores   | subset |     5 | n         |  1.28722653275382 |  0.0462510925214765 |       1.216796875 |  1.35671936758893 |
| volrend   | nodes   | none   |     7 | n         |  1.03510226382877 |  0.0104906990448843 |  1.01410437235543 |  1.04992867332382 |
| word_count | cores   | subset |     4 | n         |  1.50475186815927 |  0.0304093755192719 |  1.46606786427146 |  1.56072874493927 |
| word_count | cores   | subset |     3 | n         |  1.73456826348553 |  0.0735805366700919 |  1.65181347150259 |  1.86512118018967 |
| word_count | cores   | subset |     2 | n         |  1.90265258161868 |  0.0558897918350125 |  1.81561822125813 |  1.98795180722892 |
| word_count | cores   | none   |     8 | n         |   1.0761863047426 | 0.00748119667274538 |  1.06155218554862 |   1.0903342366757 |
| word_count | cores   | none   |     7 | n         |  1.08597648633607 | 0.00963325701279409 |  1.06928838951311 |   1.1038715769594 |
| word_count | cores   | none   |     6 | n         |  1.10651994974292 |  0.0370540623317011 |  1.04293893129771 |  1.15764023210832 |
| word_count | cores   | none   |     5 | n         |  1.12496901839177 |  0.0217096711241581 |       1.087890625 |   1.1600790513834 |
| word_count | cores   | none   |     4 | n         |  1.25873927587245 |  0.0430008977543231 |  1.20059880239521 |  1.32287449392713 |
| word_count | cores   | none   |     3 | n         |  1.29418429077872 |  0.0417064854394803 |  1.24766839378238 |  1.36986301369863 |
| word_count | cores   | none   |     2 | n         |  1.26544346230299 |   0.220653146595803 |  1.09652928416486 |  1.64950711938664 |
| volrend   | cores   | subset |     8 | n         |  1.09872674139755 | 0.00651331408978544 |  1.08459422283356 |  1.11111111111111 |
| volrend   | cores   | subset |     7 | n         |  1.12550646197418 |  0.0195990933567859 |  1.09861212563915 |  1.15276752767528 |
| bodytrack | nodes   | none   |     3 | n         | 0.942316910802016 |  0.0132975086868944 | 0.924546952224053 | 0.969010329890037 |
| volrend   | cores   | subset |     6 | n         |  1.26891300908062 |   0.129262449781631 |  1.09216589861751 |  1.44728682170543 |
| volrend   | cores   | subset |     5 | n         |  1.17925205642076 |   0.164233234204125 |  1.04780876494024 |  1.45826645264848 |
| volrend   | cores   | subset |     4 | n         |  1.08958117420819 |  0.0182892018443002 |  1.05685358255452 |  1.10901960784314 |
| volrend   | cores   | subset |     3 | n         |  1.07109106606871 |  0.0161022613686934 |  1.04285714285714 |  1.08739255014327 |
| volrend   | cores   | subset |     2 | n         |  1.02198665330829 | 0.00357237519121843 |  1.01383285302594 |  1.02913752913753 |
| volrend   | cores   | none   |     8 | n         |  1.09317586427019 |  0.0201284376750111 |   1.0646492434663 |  1.12648497554158 |
| volrend   | cores   | none   |     7 | n         |  1.07244424867719 |  0.0107313754142328 |  1.05040175310446 |  1.08634686346863 |
| volrend   | cores   | none   |     6 | n         |  1.05556278286548 | 0.00868794060366419 |  1.04454685099846 |  1.07441860465116 |
| volrend   | cores   | none   |     5 | n         |  1.06908456183144 | 0.00456170967847716 |  1.06294820717131 |  1.07945425361156 |
| volrend   | cores   | none   |     4 | n         |   1.0243274334541 | 0.00726543381093137 |  1.01323987538941 |  1.03843137254902 |
| volrend   | cores   | none   |     3 | n         |  1.00921136847984 | 0.00291447794534474 |  1.00357142857143 |  1.01361031518625 |
| volrend   | cores   | none   |     2 | n         |  1.00008175839707 | 0.00925743322364941 |  0.98328530259366 |  1.01806526806527 |
| histogram | cores   | subset |     8 | n         |  1.20083408983645 |  0.0329419294678034 |  1.14054054054054 |  1.24043715846995 |
| histogram | cores   | subset |     7 | n         |  1.23195924327815 |  0.0216153662382047 |  1.18235294117647 |  1.25748502994012 |
| histogram | cores   | subset |     6 | n         |  1.31836913086913 |  0.0902602445115628 |  1.21153846153846 |  1.47402597402597 |
| histogram | cores   | subset |     5 | n         |  1.32816096387596 |  0.0645297876579249 |   1.2551724137931 |   1.4468085106383 |
| histogram | cores   | subset |     4 | n         |  1.68610152604912 |  0.0756352785905282 |  1.54285714285714 |  1.79850746268657 |
| histogram | cores   | subset |     3 | n         |  1.45429266663514 |   0.187425625086626 |  1.18918918918919 |  1.73793103448276 |
| histogram | cores   | subset |     2 | n         |  1.30187719095574 |  0.0993751603408374 |  1.11052631578947 |  1.47337278106509 |
| histogram | cores   | none   |     8 | n         |  1.21036451912087 |  0.0270465611769665 |  1.15675675675676 |  1.23497267759563 |
| histogram | cores   | none   |     7 | n         |  1.17826610434593 |  0.0254231509674047 |  1.12941176470588 |  1.21556886227545 |
| histogram | cores   | none   |     6 | n         |  1.16884677822178 |   0.029019947158736 |  1.12179487179487 |  1.20779220779221 |
| histogram | cores   | none   |     5 | n         |  1.21733428742008 |  0.0573446153979844 |  1.11034482758621 |  1.28368794326241 |
| histogram | cores   | none   |     4 | n         |  1.20200953861357 |   0.105093830164489 |  1.05714285714286 |  1.38059701492537 |
| histogram | cores   | none   |     3 | n         |  1.18225439134692 |   0.138436492934036 |  1.06756756756757 |  1.42758620689655 |
| histogram | cores   | none   |     2 | n         | 0.898194341046986 |  0.0793190638091176 | 0.731578947368421 |  1.01775147928994 |
| bodytrack | cores   | subset |     8 | n         | 0.903432931188048 |  0.0109016226780611 |  0.88897426046869 | 0.926098794243485 |
| bodytrack | cores   | subset |     7 | n         | 0.915961057623961 |  0.0125676960179536 | 0.902665121668598 | 0.938593082005441 |
| bodytrack | cores   | subset |     6 | n         | 0.916738653619362 | 0.00736285703812947 | 0.904417364813404 | 0.930053804765565 |
| bodytrack | cores   | subset |     5 | n         | 0.920118535391682 |  0.0171699931013025 | 0.872241992882562 | 0.940203083866115 |
| bodytrack | cores   | subset |     4 | n         | 0.937624273384065 | 0.00437383431299313 | 0.929830747531735 | 0.947518743305962 |
| bodytrack | cores   | subset |     3 | n         | 0.958751658703951 | 0.00939353654446348 | 0.941383728379244 | 0.970654627539503 |
| bodytrack | cores   | subset |     2 | n         | 0.990669647664285 | 0.00297167668867566 | 0.985090521831736 |  0.99519615692554 |
| bodytrack | cores   | none   |     8 | n         | 0.895903519003769 |  0.0038758106101546 |  0.88820591625048 | 0.904706339945547 |
| bodytrack | cores   | none   |     7 | n         | 0.890951220300754 | 0.00456838821859672 | 0.881421398223252 | 0.897784687135639 |
| word_count | nodes   | subset |     6 | n         |  1.16636184740293 |  0.0490759963405948 |  1.06654676258993 |  1.24590163934426 |
| bodytrack | cores   | none   |     6 | n         | 0.894054684011115 | 0.00516386131128961 | 0.882711348057883 | 0.902382782475019 |
| bodytrack | cores   | none   |     5 | n         | 0.898527810175654 |  0.0153490477219297 | 0.858007117437722 |  0.90898834148176 |
| bodytrack | cores   | none   |     4 | n         | 0.903061563873689 | 0.00452604095125878 | 0.895275035260931 |  0.91253123884327 |
| bodytrack | cores   | none   |     3 | n         |  0.91356689528102 |  0.0124594700854095 | 0.898782831518258 | 0.937762012254111 |
| bodytrack | cores   | none   |     2 | n         | 0.916089498596986 | 0.00597469124881046 |  0.90814696485623 | 0.924472911662664 |

** Plot

#+BEGIN_SRC R :results output graphics :file speedup.png
  ggplot(processed,
    x = Nodes,
    y = SpeedupMean
  ) +
    geom_line(aes(
      x = Nodes,
      y = SpeedupMean,
      colour = Pinning
    )) +
    geom_pointrange(aes(
      x = Nodes,
      y = SpeedupMean,
      ymin = SpeedupMin,
      ymax = SpeedupMax,
      colour = Pinning
    )) +
    facet_grid(Config ~ Program) +
    theme(legend.position = "bottom") +
    scale_x_continuous(breaks = seq(2, 8, by=1)) +
    ylim(0, NA)
#+END_SRC

#+RESULTS:
[[file:speedup.png]]

