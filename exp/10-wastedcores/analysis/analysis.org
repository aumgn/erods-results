#+TITLE: Wasted Cores
#+AUTHOR: Christopher Ferreira

* Gather data

#+BEGIN_SRC sh
  export LANG=C

  datafile=time.csv
  rm -f $datafile
  for exp in ../../08-baseline/wastedcores/outputs/* ../results*/outputs/*; do
      program=$(echo $(basename $exp) | cut -d'-' -f1)
      pinning=$(echo $(basename $exp) | cut -d'-' -f2)
      interleave=$(echo $(basename $exp) | cut -d'-' -f3)

      for iteration_dir in $exp/*; do
          iteration=$(basename $iteration_dir)
          time=$(tail -1 $iteration_dir/time)

          printf '%s,%s,%s,%d,%f\n' \
                 $program $pinning $interleave $iteration \
                 $time \
                 >>$datafile
      done
  done

  wc -l $datafile
#+END_SRC

#+RESULTS:
: 1720 time.csv

* R session
  :PROPERTIES:
  :colnames: yes
  :session:  *wastedcores-analysis*
  :unit: px
  :END:

*** Load libraries

#+BEGIN_SRC R :colnames no :results value verbatim
  library('tidyverse')
  library('ggthemr')

  'Ok !'
#+END_SRC

#+RESULTS:
: Ok !

*** Load data

#+BEGIN_SRC R
  data <- read_csv('time.csv',
          col_names = c('Program', 'Pinning', 'Interleave', 'Iteration', 'Time')) %>%
          filter(Iteration <= 20)
  summary(data)
#+END_SRC

#+RESULTS:
| Program          | Pinning          | Interleave       | Iteration    | Time       |
|------------------+------------------+------------------+--------------+------------|
| Length:1280      | Length:1280      | Length:1280      | Min.   : 1.0 | Min.   : 0 |
| Class :character | Class :character | Class :character | 1st Qu.: 5.8 | 1st Qu.: 6 |
| Mode  :character | Mode  :character | Mode  :character | Median :10.5 | Median :11 |
| nil              | nil              | nil              | Mean   :10.5 | Mean   :12 |
| nil              | nil              | nil              | 3rd Qu.:15.2 | 3rd Qu.:16 |
| nil              | nil              | nil              | Max.   :20.0 | Max.   :49 |

*** Process Data

#+BEGIN_SRC R
  options(digits=2)

  relevant <- data %>%
      select(-Iteration)

  means <- relevant %>%
      group_by(Program, Pinning, Interleave) %>%
      summarise(TimeMean = mean(Time), TimeSd = sd(Time)) %>%
      ungroup() %>%
      arrange(Program, Interleave, Pinning)

  ratios <- means %>% mutate(
       TimeRef     = rep(TimeMean[Pinning == 'cores' & Interleave == 'default'], each = 4),
       TimeRatio   = TimeMean / TimeRef,
       TimeRatioSd = TimeSd / TimeRef
  )
#+END_SRC

#+RESULTS:
| Program       | Pinning | Interleave | TimeMean |              TimeSd | TimeRef |         TimeRatio |          TimeRatioSd |
|---------------+---------+------------+----------+---------------------+---------+-------------------+----------------------|
| barnes        | cores   | default    |  11.4815 |   0.184541394586118 | 11.4815 |                 1 |   0.0160729342495422 |
| barnes        | none    | default    |   11.476 |  0.0763923391033697 | 11.4815 | 0.999520968514567 |  0.00665351557752643 |
| barnes        | cores   | interleave |   11.099 |   0.105176543312461 | 11.4815 | 0.966685537603972 |  0.00916052286830646 |
| barnes        | none    | interleave |   11.245 |   0.182223113676325 | 11.4815 | 0.979401646126377 |   0.0158710197862932 |
| bodytrack     | cores   | default    |   25.647 |   0.596684700305737 |  25.647 |                 1 |    0.023265282501101 |
| bodytrack     | none    | default    |  23.0625 |  0.0866860639561549 |  25.647 | 0.899227979880688 |  0.00337996896152201 |
| bodytrack     | cores   | interleave |  25.7175 |  0.0938573946277852 |  25.647 |  1.00274885951573 |  0.00365958570701389 |
| bodytrack     | none    | interleave |  23.2235 |   0.250016315257098 |  25.647 | 0.905505517214489 |  0.00974836492599907 |
| dedup_app     | cores   | default    |    5.804 |  0.0298063928148238 |   5.804 |                 1 |  0.00513549152564159 |
| dedup_app     | none    | default    |   5.7985 |   0.111084368126687 |   5.804 | 0.999052377670572 |   0.0191392777613175 |
| dedup_app     | cores   | interleave |   6.4405 |  0.0433437909108826 |   5.804 |  1.10966574776017 |  0.00746791711076544 |
| dedup_app     | none    | interleave |   6.3685 |  0.0728571060002855 |   5.804 |  1.09726050999311 |   0.0125529128187949 |
| dedup_pool    | cores   | default    |   7.1555 |  0.0708946586956941 |  7.1555 |                 1 |  0.00990771556085446 |
| dedup_pool    | none    | default    |    7.227 |  0.0754006840678169 |  7.1555 |  1.00999231360492 |   0.0105374444927422 |
| dedup_pool    | cores   | interleave |    7.912 |   0.102217518325537 |  7.1555 |  1.10572287051918 |   0.0142851678185364 |
| dedup_pool    | none    | interleave |    7.907 |   0.067986066683975 |  7.1555 |  1.10502410733003 |  0.00950123215484243 |
| fluidanimate  | cores   | default    |   47.991 |   0.202559932651522 |  47.991 |                 1 |  0.00422078999503078 |
| fluidanimate  | none    | default    |  47.5255 |   0.912062295887154 |  47.991 | 0.990300264632952 |   0.0190048612424653 |
| fluidanimate  | cores   | interleave |    33.37 |   0.184105233065509 |  47.991 | 0.695338709341335 |  0.00383624498479942 |
| fluidanimate  | none    | interleave |  33.1755 |     0.1470221966772 |  47.991 | 0.691285866099894 |  0.00306353684393324 |
| histogram_mr  | cores   | default    |   1.8565 |  0.0130887657735053 |  1.8565 |                 1 |  0.00705023742176423 |
| histogram_mr  | none    | default    |    1.862 |  0.0147255595908324 |  1.8565 |  1.00296256396445 |  0.00793189312729998 |
| histogram_mr  | cores   | interleave |   1.9035 |  0.0134848843251678 |  1.8565 |   1.0253164556962 |  0.00726360588481974 |
| histogram_mr  | none    | interleave |    1.913 |   0.011742858972248 |  1.8565 |   1.0304336116348 |  0.00632526742378023 |
| lin_reg_mr    | cores   | default    |    0.494 | 0.00753937034925053 |   0.494 |                 1 |    0.015261883298078 |
| lin_reg_mr    | none    | default    |   0.4885 |   0.005871429486124 |   0.494 | 0.988866396761134 |   0.0118854847897247 |
| lin_reg_mr    | cores   | interleave |   0.5045 |  0.0068633274115326 |   0.494 |  1.02125506072874 |   0.0138933753269891 |
| lin_reg_mr    | none    | interleave |   0.4965 | 0.00670820393249937 |   0.494 |  1.00506072874494 |   0.0135793601872457 |
| lin_reg_pt    | cores   | default    |     2.38 |  0.0319539141824433 |    2.38 |                 1 |   0.0134260143623711 |
| lin_reg_pt    | none    | default    |    3.934 |   0.449846757533227 |    2.38 |  1.65294117647059 |     0.18901124266102 |
| lin_reg_pt    | cores   | interleave |   2.3535 |   0.086101225616315 |    2.38 | 0.988865546218487 |   0.0361769855530735 |
| lin_reg_pt    | none    | interleave |   4.0105 |   0.410474372557308 |    2.38 |  1.68508403361345 |    0.172468223763575 |
| lu_cb         | cores   | default    |  19.8295 |   0.263268424156592 | 19.8295 |                 1 |   0.0132766042591387 |
| lu_cb         | none    | default    |  19.7285 |   0.528496128552269 | 19.8295 | 0.994906578582415 |   0.0266520148542459 |
| lu_cb         | cores   | interleave |  13.4555 |    0.22779203350055 | 19.8295 | 0.678559721626869 |   0.0114875328929398 |
| lu_cb         | none    | interleave |    13.46 |   0.243007472269779 | 19.8295 | 0.678786656244484 |   0.0122548461771492 |
| lu_ncb        | cores   | default    |  18.6845 |  0.0170061908232201 | 18.6845 |                 1 |  0.00091017639343949 |
| lu_ncb        | none    | default    |  23.7525 |    1.51543940127851 | 18.6845 |  1.27124086809923 |   0.0811067677100543 |
| lu_ncb        | cores   | interleave |   11.079 |  0.0878934761964433 | 18.6845 | 0.592951376809655 |  0.00470408500074625 |
| lu_ncb        | none    | interleave |  11.9215 |   0.567573551922661 | 18.6845 | 0.638042227514785 |   0.0303767053933828 |
| ocean_cp      | cores   | default    |   16.597 |    0.13243071674216 |  16.597 |                 1 |  0.00797919604399349 |
| ocean_cp      | none    | default    |   17.219 |   0.712718295275947 |  16.597 |  1.03747665240706 |   0.0429425977752574 |
| ocean_cp      | cores   | interleave |   6.9385 |  0.0384262793849263 |  16.597 | 0.418057480267518 |  0.00231525452701852 |
| ocean_cp      | none    | interleave |     7.05 |  0.0148678388335006 |  16.597 | 0.424775561848527 | 0.000895814836024618 |
| pca_pt        | cores   | default    |  13.0995 |   0.156153601842612 | 13.0995 |                 1 |    0.011920577261927 |
| pca_pt        | none    | default    |  13.1245 |   0.452472447547332 | 13.0995 |  1.00190846978892 |   0.0345411998585696 |
| pca_pt        | cores   | interleave |   10.392 |   0.153266194301905 | 13.0995 | 0.793312721859613 |    0.011700156059537 |
| pca_pt        | none    | interleave |   10.377 |  0.0886803841123605 | 13.0995 | 0.792167639986259 |  0.00676975335794195 |
| vips          | cores   | default    |   5.8975 |  0.0352248417126922 |  5.8975 |                 1 |  0.00597284302038019 |
| vips          | none    | default    |   5.8605 |  0.0428553257760155 |  5.8975 | 0.993726155150487 |  0.00726669364578473 |
| vips          | cores   | interleave |    6.499 |  0.0444735284955231 |  5.8975 |  1.10199236964816 |  0.00754108155922392 |
| vips          | none    | interleave |   6.3775 |  0.0222130829159659 |  5.8975 |  1.08139041966935 |  0.00376652529308451 |
| volrend       | cores   | default    |   14.581 |  0.0528055021056669 |  14.581 |                 1 |  0.00362152816032281 |
| volrend       | none    | default    |  14.6835 |  0.0413298668608654 |  14.581 |  1.00702969617996 |  0.00283450153356185 |
| volrend       | cores   | interleave |   14.568 |  0.0564381356699063 |  14.581 | 0.999108428777176 |  0.00387066289485675 |
| volrend       | none    | interleave |   14.681 |  0.0273187808928891 |  14.581 |  1.00685824017557 |  0.00187358760667232 |
| word_count_mr | cores   | default    |   15.214 |  0.0630288154652091 |  15.214 |                 1 |  0.00414281684403898 |
| word_count_mr | none    | default    |   15.469 |  0.0896719166271076 |  15.214 |  1.01676087813856 |  0.00589403947857944 |
| word_count_mr | cores   | interleave |   15.914 |  0.0484930814381642 |  15.214 |  1.04601025371368 |  0.00318739854332616 |
| word_count_mr | none    | interleave |   16.018 |  0.0779068269422555 |  15.214 |  1.05284606283686 |  0.00512073267663044 |
| word_count_pt | cores   | default    |    8.664 |  0.0239297216646449 |   8.664 |                 1 |  0.00276197156794147 |
| word_count_pt | none    | default    |    8.706 |   0.136049526585764 |   8.664 |  1.00484764542936 |   0.0157028539457253 |
| word_count_pt | cores   | interleave |    8.695 |  0.0439497320511565 |   8.664 |  1.00357802400739 |  0.00507268375475028 |
| word_count_pt | none    | interleave |    8.746 |    0.15588119632727 |   8.664 |  1.00946445060018 |   0.0179918278309407 |

*** Plot utils

#+BEGIN_SRC R :colnames no :results value verbatim
    custom_theme <- ggthemr('flat', set_theme = FALSE)$theme + theme(
      plot.title = element_blank(),
      plot.margin = unit(c(0.2, 0.1, 0.2, 0.1), 'cm'),
      legend.title = element_blank(),
      legend.position = 'top',
      legend.text = element_text(face = 'bold', size = 12),
      legend.spacing.x = unit(1.6, 'cm'),
      legend.key.size = unit(0.7, 'cm'),
      axis.title = element_blank(),
      axis.line.x = element_blank(),
      axis.text.x = element_blank(),
      axis.text.y = element_text(face = 'bold', size = 12),
      panel.grid.major.x = element_blank(),
      panel.spacing = unit(0.1, 'cm'),
      complete = FALSE
    )

    'Ok'
#+END_SRC

#+RESULTS:
: Ok

*** Plot

#+BEGIN_SRC R :results output graphics :width 900 :height 800 :file time.png
  dodge <- position_dodge(width = 0.9)
  labels <- c('PinThreads & First Touch', 'PinThreads & Interleave', 'CFS & First Touch', 'CFS & Interleave')
  palette <- 'Set2'

  ratios %>%
    unite(PI, Pinning, Interleave) %>%
    ggplot(aes(
      x = PI,
      y = TimeRatio,
      fill = PI
    )) +
      geom_bar(position = dodge, stat = 'identity', aes(
          colour = PI,
      )) +
      geom_errorbar(position = dodge, aes(
        ymin = TimeRatio - TimeRatioSd,
        ymax = TimeRatio + TimeRatioSd,
        width = 0.1
      )) +
      # scale_y_continuous(limits = c(0, 1.7)) +
      scale_colour_hue(labels = labels) +
      scale_fill_hue(labels = labels) +
      custom_theme +
      facet_wrap(~ Program)
#+END_SRC

#+RESULTS:
[[file:time.png]]


