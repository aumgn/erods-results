#! /usr/bin/python

import sys
import schedstatparser as parser
import schedstatnames  as names

cpus = parser.parse(sys.argv[1])

for cpu in cpus.values():
    for domain in cpu.domains.values():
        mask = ''.join([ hex(m)[2:].zfill(8) for m in domain.cpusmask ])
        for i, stat in enumerate(domain.stats):
            if names.table[i]:
                print("{},{},{},{},{}".format(
                    cpu.index,
                    domain.index,
                    mask,
                    names.table[i],
                    stat
                ))

