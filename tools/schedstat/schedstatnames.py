# References :
#   - http://elixir.free-electrons.com/linux/v4.9.9/source/include/linux/sched.h#L1110
#   - http://elixir.free-electrons.com/linux/v4.9.9/source/Documentation/scheduler/sched-stats.txt
#   - http://elixir.free-electrons.com/linux/v4.9.9/source/kernel/sched/stats.c

table = [

    ### load_balance() stats when idle (CPU_IDLE)

    # # of times in this domain load_balance() was called when the
    # cpu was idle
    "idle_lb_count",

    # # of times in this domain load_balance() checked but found
    # the load did not require balancing when the cpu was idle
    "idle_lb_balanced",

    # # of times in this domain load_balance() tried to move one or
    # more tasks and failed, when the cpu was idle
    "idle_lb_failed",

    # sum of imbalances discovered (if any) with each call to
    # load_balance() in this domain when the cpu was idle
    "idle_lb_imbalance",

    # # of times in this domain pull_task() was called when the cpu
    # was idle
    "idle_lb_gained",

    # # of times in this domain pull_task() was called even though
    # the target task was cache-hot when idle
    "idle_lb_hot_gained",

    # # of times in this domain load_balance() was called but did
    # not find a busier queue while the cpu was idle
    "idle_lb_nobusyq",

    # # of times in this domain a busier queue was found while the
    # cpu was idle but no busier group was found
    "idle_lb_nobusyg",

    ### load_balance() stats when busy (CPU_NOT_IDLE)

    # # of times in this domain load_balance() was called when the
    # cpu was busy
    "busy_lb_count",

    # # of times in this domain load_balance() checked but found the
    # load did not require balancing when busy
    "busy_lb_balanced",

    # # of times in this domain load_balance() tried to move one or
    # more tasks and failed, when the cpu was busy
    "busy_lb_failed",

    # sum of imbalances discovered (if any) with each call to
    # load_balance() in this domain when the cpu was busy
    "busy_lb_imbalance",

    # # of times in this domain pull_task() was called when busy
    "busy_lb_gained",

    # # of times in this domain pull_task() was called even though the
    # target task was cache-hot when busy
    "busy_lb_hot_gained",

    # # of times in this domain load_balance() was called but did not
    # find a busier queue while the cpu was busy
    "busy_lb_nobusyq",

    # # of times in this domain a busier queue was found while the cpu
    # was busy but no busier group was found
    "busy_lb_nobusyg",

    ### load_balance() stats when newly idle (CPU_NEWLY_IDLE)

    # # of times in this domain load_balance() was called when the
    # cpu was just becoming idle
    "newly_idle_lb_count",

    # # of times in this domain load_balance() checked but found the
    # load did not require balancing when the cpu was just becoming idle
    "newly_idle_lb_balanced",

    # # of times in this domain load_balance() tried to move one or more
    # tasks and failed, when the cpu was just becoming idle
    # was just becoming idle but no busier group was found
    "newly_idle_lb_failed",

    # sum of imbalances discovered (if any) with each call to
    "newly_idle_lb_imbalance",

    # load_balance() in this domain when the cpu was just becoming idle
    "newly_idle_lb_gained",

    # # of times in this domain pull_task() was called when newly idle
    "newly_idle_lb_hot_gained",

    # # of times in this domain pull_task() was called even though the
    # target task was cache-hot when just becoming idle
    "newly_idle_lb_nobusyq",

    # # of times in this domain load_balance() was called but did not
    # find a busier queue while the cpu was just becoming idle
    "newly_idle_lb_nobusyg",

    # # of times in this domain a busier queue was found while the cpu

    ### Active load balancing
    # # of times active_load_balance() was called
    "alb_count",
    # # of times active_load_balance() tried to move a task and failed
    "alb_failed",
    # # of times active_load_balance() successfully moved a task
    "alb_pushed",

    ### Unused
    "sbe_count",
    "sbe_balanced",
    "sbe_pushed",
    "sbf_count",
    "sbf_balanced",
    "sbf_pushed",

    ### try_to_wake_up() stats
    # # of times in this domain try_to_wake_up() awoke a task that
    # last ran on a different cpu in this domain
    "ttwu_wake_remote",
    # # of times in this domain try_to_wake_up() moved a task to the
    # waking cpu because it was cache-cold on its own cpu anyway
    "ttwu_move_affine",
    # # of times in this domain try_to_wake_up() started passive balancing
    "ttwu_move_balance",

]

