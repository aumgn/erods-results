import re
import sys

SCHEDSTAT_VERSION = 15
version_re = re.compile('^version ([0-9]+)$')

def check_version(match):
    version = int(match.group(1))
    if SCHEDSTAT_VERSION != version:
        print("Unknown version {}, expected {}".format(
            version,
            SCHEDSTAT_VERSION))
        exit(1)

empty_re = re.compile('^\s*$')

class Cpu:
    RE = re.compile('^cpu([0-9]+) ([\-0-9 ]+)$')
    def __init__(self, match):
        self.index = int(match.group(1))
        self.stats = [ int(stat) for stat in match.group(2).split(" ") ]
        self.domains = {}

class Domain:
    RE = re.compile('^domain([0-9]+) ([\-0-9a-f,]+) ([\-0-9 ]+)$')
    def __init__(self, match):
        self.index = int(match.group(1))
        self.cpusmask = [ int(part, 16) for part in match.group(2).split(",") ]
        self.stats = [ int(stat) for stat in match.group(3).split(" ") ]

def parse(filename):
    cpus = {}
    with open(filename) as f:
        cpu = None
        domain = None
        for line in f:
            version_match = version_re.match(line)
            if version_match:
                check_version(version_match)
                continue

            empty_match = empty_re.match(line)
            if empty_match:
                continue

            cpu_match = Cpu.RE.match(line)
            if cpu_match:
                cpu = Cpu(cpu_match)
                cpus[cpu.index] = cpu
                continue

            domain_match = Domain.RE.match(line)
            if domain_match:
                domain = Domain(domain_match)
                cpu.domains[domain.index] = domain
                continue
    return cpus

