#! /usr/bin/python

import sys
import schedstatparser as parser

cpus1 = parser.parse(sys.argv[1])
cpus2 = parser.parse(sys.argv[2])

print("version {}".format(parser.SCHEDSTAT_VERSION))
print("timestamp 0")

for cpu1 in cpus1.values():
    cpu2 = cpus2[cpu1.index]

    print("cpu{} {}".format(
        cpu1.index,
        ' '.join([ str(stat2 - stat1) for (stat1, stat2) in zip(cpu1.stats, cpu2.stats) ])
    ))

    for domain1 in cpu1.domains.values():
        domain2 = cpu2.domains[domain1.index]

        print("domain{} {} {}".format(
            domain1.index,
            ','.join([ hex(m)[2:].zfill(8) for m in domain1.cpusmask ]),
            ' '.join([ str(stat2 - stat1) for (stat1, stat2) in zip(domain1.stats, domain2.stats) ])
            ))

