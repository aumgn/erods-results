library('tidyverse')
library('ggthemr')

light_theme <- ggthemr('flat', set_theme = FALSE)
dark_theme <- ggthemr('flat dark', set_theme = FALSE)

args = commandArgs(trailingOnly=TRUE)
if (length(args) != 1) {
  stop("Exactly one argument required (csv input)", call.=FALSE)
}

migrations <- read_csv(args[1], col_names = TRUE) %>%
    filter(Trigger %in% c('NLB', 'SLB', 'ILB', 'WA', 'WLB')) %>%
    group_by(Orig, Dest) %>%
    summarize(Count = n()) %>%
    ungroup()

cpus_x_breaks <- seq(0, 63)
cpus_y_breaks <- seq(0, 63)

png('migrations_matrix.png', width = 940, height = 900)
ggplot(migrations, aes(x = Dest, y = Orig)) +
    geom_tile(aes(fill = Count)) +
    scale_x_continuous(breaks = cpus_x_breaks, position = 'top', expand = c(0, 0)) +
    scale_y_continuous(breaks = cpus_y_breaks, trans = "reverse", expand = c(0, 0)) +
    scale_fill_distiller(palette = "Spectral") +

    xlab("Destination CPU") +
    ylab("Origin CPU") +

    theme(
        line = element_blank(),
        plot.margin = unit(c(0.2, 0.2, 0.2, 0.2), 'cm'),
        legend.text = element_blank()
    )

