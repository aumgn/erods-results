# -*- coding: utf-8 -*-

import os
import sys
import getopt

sys.path.append(os.environ['PERF_EXEC_PATH'] + \
	'/scripts/python/Perf-Trace-Util/lib/Perf/Trace')

from perf_trace_context import *
from Core import *

import csv

TRIGGER_SLB  = 0x0001
TRIGGER_NLB  = 0x0002
TRIGGER_ILB  = 0x0004
TRIGGER_WLB  = 0x0008
TRIGGER_WAFF = 0x0010
TRIGGER_FLB  = 0x0020
TRIGGER_ELB  = 0x0040
TRIGGER_RT   = 0x0080
TRIGGER_DL   = 0x0100
TRIGGER_NB   = 0x0200
TRIGGER_CA   = 0x0400
TRIGGER_HP   = 0x0800
TRIGGER_OTH  = 0x1000
TRIGGER_EXIT = 0x8000

TRIGGER_SYMBOLS = {
    TRIGGER_SLB  : 'SLB',
    TRIGGER_NLB  : 'NLB',
    TRIGGER_ILB  : 'ILB',
    TRIGGER_WLB  : 'WLB',
    TRIGGER_WAFF : 'WAFF',
    TRIGGER_FLB  : 'FLB',
    TRIGGER_ELB  : 'ELB',
    TRIGGER_RT   : 'RT',
    TRIGGER_DL   : 'DL',
    TRIGGER_NB   : 'NB',
    TRIGGER_CA   : 'CA',
    TRIGGER_HP   : 'HP',
    TRIGGER_OTH  : 'OTH',
    TRIGGER_EXIT : 'EXIT'
}


data = None

class GlobalData:
    def __init__(self):
        if len(sys.argv) != 5:
            print("Invalid number of arguments expected 4, got {}".format(len(sys.argv) - 1))
            print("perf script ... <comm> <exec> <migrations-output> <rq-sizes-output>")
            exit(15)

        self.comm = sys.argv[1]
        self.exec_comm = sys.argv[2]
        self.last_secs = 0
        self.rq_start = [None] * 64
        self.rq_sizes = [0] * 64

        try:
            self.migrations_file = open(sys.argv[3], 'w')
            self.rq_sizes_file = open(sys.argv[4], 'w')
        except:
            exit(10)

        self.migrations_writer = csv.writer(self.migrations_file)
        self.migrations_writer.writerow(['Time', 'Comm', 'Pid', 'Orig', 'Dest', 'Trigger'])
        self.rq_sizes_writer = csv.writer(self.rq_sizes_file)
        self.rq_sizes_writer.writerow(['Start', 'End', 'Cpu', 'Size'])


    def perf_clean_up_comm(self, comm, trigger):
        # Perf python lib seems to mess up the translation of the comm
        # fields leaving them with some sort of trailing 0 followed by trash
        for i, c in enumerate(comm):
            if c == 0:
                comm = comm[:i]
                break

        if comm == self.exec_comm and trigger == TRIGGER_ELB:
            comm = self.comm

        return comm

    def perf_timestamp_format(self, secs, nsecs):
        return "{}.{:0>9d}".format(secs, nsecs)


    def process(self, secs, nsecs, comm, pid, trigger, orig_cpu, dest_cpu):
        comm = self.perf_clean_up_comm(comm, trigger)
        now = self.perf_timestamp_format(secs, nsecs)

        if trigger != TRIGGER_EXIT:
            self.migrations_writer.writerow([
                now, comm, pid, orig_cpu, dest_cpu, TRIGGER_SYMBOLS.get(trigger)
            ])

        if comm != self.comm:
            return

        if self.last_secs < secs:
            self.last_secs = secs + 1

        if trigger not in (TRIGGER_ELB, TRIGGER_FLB):
            start    = self.rq_start[orig_cpu]
            orig_was = self.rq_sizes[orig_cpu]
            orig_now = orig_was - 1

            self.rq_start[orig_cpu] = now
            self.rq_sizes[orig_cpu] = orig_now
            if orig_was > 0:
                self.rq_sizes_writer.writerow([start, now, orig_cpu, orig_was])

        if trigger != TRIGGER_EXIT:
            self.migrations_writer.writerow([now, comm, pid, orig_cpu, dest_cpu,
                                             TRIGGER_SYMBOLS.get(trigger)])

            start    = self.rq_start[dest_cpu]
            dest_was = self.rq_sizes[dest_cpu]
            dest_now = dest_was + 1

            self.rq_start[dest_cpu] = now
            self.rq_sizes[dest_cpu] = dest_now
            if dest_was > 0:
                self.rq_sizes_writer.writerow([start, now, dest_cpu, dest_was])


def trace_begin():
    global data
    data = GlobalData()

def trace_end():
    for cpu in range(64):
        data.process(data.last_secs, 0, data.comm, 0, TRIGGER_EXIT, cpu, -1)
    data.migrations_file.close()
    data.rq_sizes_file.close()

def sched__sched_thread_placement(event_name, context, common_cpu,
	common_secs, common_nsecs, common_pid, common_comm,
	common_callchain, comm, pid, orig_cpu, dest_cpu,
	exec_cpu, trigger, level, task_load):
    data.process(common_secs, common_nsecs, comm, pid, trigger, orig_cpu, dest_cpu)

def sched__sched_process_exit(event_name, context, common_cpu,
	common_secs, common_nsecs, common_pid, common_comm,
	common_callchain, comm, pid, prio):
    data.process(common_secs, common_nsecs, comm, pid, TRIGGER_EXIT, common_cpu, -1)

